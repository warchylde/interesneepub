'use strict';

angular.module('interesneeApp')
    .directive('repeatCheck', function () {
        return {
            restrict: 'A',
            scope: {
                master: '=repeatCheck',
                model: '=ngModel'
            },
            require: 'ngModel',
            link: function(scope, element, attrs, ngModelCtrl) {
                scope.$watch('master', check);
                scope.$watch('model', check);

                function check(value) {
                    ngModelCtrl.$setValidity('repeat-check', scope.master == scope.model);
                }
            }
        }
    });