(function() {
    'use strict';

    angular
        .module('interesneeApp')
        .directive('allowMaxlength', AllowMaxlengthDirective)
    ;

    AllowMaxlengthDirective.$inject = ['$parse'];
    function AllowMaxlengthDirective($parse) {
        return {
            link: link
        };

        function link($scope, $element, $attr) {
            var maxlength = parseInt($attr.allowMaxlength);
            var model = $attr.ngModel;

            if(model && !isNaN(maxlength)) {
                var parsed = $parse(model);

                $scope.$watch($attr.ngModel, function (value) {
                    if(angular.isString(value) && value.length > maxlength) {
                        parsed.assign($scope, value.substr(0, maxlength));
                    }
                });
            }
        }
    }

})();
