(function () {
    'use strict';

    angular
        .module('interesneeApp', [
            'ngFx',
            'ngAnimate',
            'ngMessages',
            'ui.router',
            'ui.mask',
            'ui.bootstrap',
            'LocalStorageModule'
        ])
        .provider('Options', function OptionsProvider() {
            this.$get = function () {
                return {
                    steps: {
                        '1': {
                            title: 'Авторизация',
                            template: 'views/step1.auth.html',
                            options: {
                                login: 'Логин',
                                email: 'Email',
                                password: 'Пароль',
                                repassword: 'Подтверждение пароля'
                            }
                        },
                        '2': {
                            title: 'Персональные данные',
                            template: 'views/step2.persdata.html',
                            options: {
                                name: 'Имя',
                                surname: 'Фамилия',
                                birthdate: 'День рождения',
                                gender: {
                                    title: 'Пол',
                                    choices: [
                                        {id: 'male', label: 'Мужской'},
                                        {id: 'female', label: 'Женский'}
                                    ]
                                },
                                additional: 'Дополнительная информация'
                            }
                        },
                        '3': {
                            title: 'Контактная информация',
                            template: 'views/step3.contacts.html',
                            options: {
                                phone: 'Телефон',
                                state: 'Страна',
                                city: 'Город',
                                address: 'Адрес',
                                address2: 'Адрес 2',
                                socnet: {
                                    title: 'Социальные сети',
                                    choices: [
                                        {id: 'vk', label: 'Vkontakte'},
                                        {id: 'fb', label: 'Facebook'},
                                        {id: 'tw', label: 'Twitter'},
                                        {id: 'ig', label: 'Instagram'},
                                        {id: 'gl', label: 'Google'}
                                    ]
                                }
                            }
                        },
                        '4': {
                            title: 'Отправка данных',
                            template: 'views/step4.submit.html',
                            options: {
                                '@hidden': true,
                                agree: {
                                    label: 'Подтверждение правил',
                                    text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus a, commodo non, facilisis vitae, nulla. Aenean dictum lacinia tortor. Nunc iaculis, nibh non iaculis aliquam, orci felis euismod neque, sed ornare massa mauris sed velit. Nulla pretium mi et risus. Fusce mi pede, tempor id, cursus ac, ullamcorper nec, enim. Sed tortor. Curabitur molestie. Duis velit augue, condimentum at, ultrices a, luctus ut, orci. Donec pellentesque egestas eros. Integer cursus, augue in cursus faucibus, eros pede bibendum sem, in tempus tellus justo quis ligula. Etiam eget tortor. Vestibulum rutrum, est ut placerat elementum, lectus nisl aliquam velit, tempor aliquam eros nunc nonummy metus. In eros metus, gravida a, gravida sed, lobortis id, turpis. Ut ultrices, ipsum at venenatis fringilla, sem nulla lacinia tellus, eget aliquet turpis mauris non enim. Nam turpis. Suspendisse lacinia. Curabitur ac tortor ut ipsum egestas elementum. Nunc imperdiet gravida mauris.'
                                }
                            }
                        }
                    }
                };
            };
        })
        .config(function ($stateProvider, $urlRouterProvider, OptionsProvider, localStorageServiceProvider) {
            $urlRouterProvider.otherwise("/step/1");

            $stateProvider.state('step', {
                url: '/step/{step}',
                controller: 'StepCtrl',
                templateUrl: function ($stateParams) {
                    var steps = OptionsProvider.$get().steps;
                    $stateParams.step = $stateParams.step in steps ? $stateParams.step : firstKey(steps);

                    return steps[$stateParams.step].template;
                }
            });

            localStorageServiceProvider.setPrefix('step');
        })
    ;
})();
