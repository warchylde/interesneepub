(function () {
    'use strict';

    angular
        .module('interesneeApp')
        .controller('StepCtrl', StepCtrl)
    ;

    StepCtrl.$inject = ['$scope', '$state', '$stateParams', 'Steps', '$filter', '$modal', 'Utils'];
    function StepCtrl($scope, $state, $stateParams, Steps, $filter, $modal, Utils) {
        var collection;

        $scope.step = Steps.get($stateParams.step);
        $scope.data = Steps.data($scope.step);

        $scope.submit = submit;
        $scope.title = title;
        $scope.subtitle = subtitle;
        $scope.value = value;
        $scope.coll = coll;
        $scope.isObject = isObject;
        $scope.isError = isError;

        function isError(field) {
            return $scope.submitted && $scope.form[field].$invalid;
        }

        function submit(step, data) {
            $scope.submitted = true;

            if ($scope.form.$valid) {
                Steps.data(step, data);
                next(step);
            }
        }

        function next(step) {
            if (step.next) {
                $state.go('step', {step: step.next.id});
            }
            else {
                $modal.open({
                    templateUrl: 'views/finalModal.html',
                    controller: ['$scope', '$modalInstance', function ($scope, $modalInstance) {
                        $scope.ok = function () {
                            $modalInstance.close();
                        };
                    }]
                });
            }

        }

        function title(field, step) {
            var step = step || $scope.step;

            return angular.isObject(step.options[field]) ? step.options[field].title : step.options[field];
        }

        function subtitle(key, field, step) {
            var step = Steps.get(step);
            var label;

            return step && step.options && step.options[field] && step.options[field].choices && (label = Utils.getBy(step.options[field].choices, key))
                ? label
                : key;
        }

        function value(value, step, field) {
            var label;

            if (angular.isObject(value)) {
                if (angular.isDate(value)) {
                    return $filter('date')(value, 'MM/dd/yyyy');
                }
                else if ('label' in value) {
                    return value.label;
                }
            }
            else if (step.options[field] && step.options[field].choices && (label = Utils.getBy(step.options[field].choices, value, {id: 'id', label: 'label'}))) {
                return label;
            }

            return value;
        }

        function coll() {
            if (!collection) {
                var steps = Steps.get();
                var fields;

                collection = [];
                angular.forEach(steps, function (step, id) {
                    if (!step.options['@hidden']) {
                        fields = [];

                        angular.forEach(step.data, function (value, id) {
                            fields.push({id: id, title: $scope.title(id, step), value: $scope.value(value, step, id)});
                        });

                        fields.length && collection.push({id: id, title: step.title, fields: fields});
                    }
                });
            }

            return collection;
        }

        function isObject(value) {
            return angular.isObject(value);
        }
    }

})();
