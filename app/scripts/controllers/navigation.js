'use strict';

angular.module('interesneeApp')
    .controller('NavigationCtrl', [
        '$scope', '$rootScope', 'Options', 'Utils', '$state', 'Steps',
        function ($scope, $rootScope, Options, Utils, $state, Steps) {
            $scope.steps = Steps.get();

            $scope.goto = function(step) {
                step.status.enabled && $state.go('step', { step: step.id });
            };

            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                var step = toParams.step;

                if (!Steps.check(step)) {
                    $state.go('step', { step: Steps.first() });
                }

                Steps.activate(step);
            });
        }
    ]);
