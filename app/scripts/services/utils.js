(function () {
    'use strict';

    angular
        .module('interesneeApp')
        .factory('Utils', function () {
            return {
                firstKey: firstKey,
                every: every,
                getBy: getBy
            };

            function getBy(coll, id) {
                var res;

                every(coll, function(obj) {
                    if(obj.id == id) {
                        res = obj.label;

                        return false;
                    }

                    return true;
                });

                return res;
            }

            function every(obj, iterator, context) {
                var key;
                if (obj) {
                    if (angular.isFunction(obj)) {
                        for (key in obj) {
                            // Need to check if hasOwnProperty exists,
                            // as on IE8 the result of querySelectorAll is an object without a hasOwnProperty function
                            if (key != 'prototype' && key != 'length' && key != 'name' && (!obj.hasOwnProperty || obj.hasOwnProperty(key))) {
                                if (!iterator.call(context, obj[key], key))
                                    return false;
                            }
                        }
                    }
                    else if (obj.every) {
                        return obj.every(iterator, context);
                    }
                    else if (isArrayLike(obj)) {
                        for (key = 0; key < obj.length; key++)
                            if (!iterator.call(context, obj[key], key))
                                return false;
                    }
                    else {
                        for (key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                if (!iterator.call(context, obj[key], key))
                                    return false;
                            }
                        }
                    }
                }
                return true;

                function isArrayLike(obj) {
                    if (obj == null || isWindow(obj)) {
                        return false;
                    }

                    var length = obj.length;

                    if (obj.nodeType === 1 && length) {
                        return true;
                    }

                    return isString(obj) || isArray(obj) || length === 0 ||
                        typeof length === 'number' && length > 0 && (length - 1) in obj;
                }

                function isWindow(obj) {
                    return obj && obj.document && obj.location && obj.alert && obj.setInterval;
                }

                function isArray(value) {
                    return toString.call(value) === '[object Array]';
                }

                function isString(value) {
                    return typeof value === 'string';
                }
            }

            function firstKey(obj) {
                for (var prop in obj) {
                    if (obj.hasOwnProperty(prop)) {
                        return prop;
                    }
                }

                return null;
            }
        })
    ;
})();
