'use strict';

angular.module('interesneeApp')
    .factory('Steps', ['Options', 'Utils', 'localStorageService', function (Options, Utils, localStorageService) {
        var steps = angular.copy(Options.steps);

        // инициализируем
        init(steps);

        return {
            get: function (step) {
                return step ? (isStep(step) ? step : steps[step]) : steps;
            },
            check: function (id) {
                var step = this.get(id);

                return step && this.enable(step);
            },
            first: function () {
                return Utils.firstKey(steps);
            },
            next: function(id) {
                var step = this.get(id);

                return step && step.next;
            },
            enable: function(id) {
                var step = this.get(id);

                return !step.prev || !!step.prev.data || !!step.data;
            },
            required: function(id) {
                var step = this.get(id);

                return (!step.prev || !!step.prev.data) && !step.data;
            },
            data: function (id, value) {
                var step = this.get(id);

                if (undefined !== value) {
                    step.data = value;
                    localStorageService.set(step.id, value);
                }

                return step.data;
            },
            activate: function (id) {
                angular.forEach(steps, function (step) {
                    step.status = {
                        active: step.id == id,
                        disabled: !this.enable(step),
                        enabled: this.enable(step),
                        required: this.required(step)
                    };
                }, this);
            }
        };

        function init(steps) {
            var prev = null;

            angular.forEach(steps, function (step, id) {
                step.id = id;
                step.data = fromLocalStorage(id);
                step.prev = prev;

                if(prev)
                    prev.next = step;

                prev = step;
            });

        }

        function isStep(step) {
            return angular.isObject(step) && step.id && step.title;
        }

        function fromLocalStorage(id) {
            var data = localStorageService.get(id);
            var dateLikeRegexp = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/;

            angular.forEach(data, function(value, key) {
                if(dateLikeRegexp.test(value)) {
                    data[key] = new Date(value);
                }
            });

            return data;
        }
    }])
;